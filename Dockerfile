FROM golang:1.10-alpine

RUN apk update && apk upgrade && \
    apk add --no-cache bash git openssh

RUN go get github.com/magefile/mage

RUN go get -d github.com/gohugoio/hugo

RUN cd /go/src/github.com/gohugoio/hugo; mage vendor
RUN cd /go/src/github.com/gohugoio/hugo; mage install
